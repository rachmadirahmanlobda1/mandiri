package xa.dokterhalo289.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

@Entity
@Table(name = "m_user")

public class M_user {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long Id;

	@ManyToOne
	@JoinColumn(name = "biodata_id", insertable = false, updatable = false)
	public M_biodata m_biodata;
	

	@ManyToOne
	@JoinColumn(name = "role_id", insertable = false, updatable = false)
	public M_role m_role;

	@Nullable
	@Column(name = "email", length = 100)
	private String Email;

	@Nullable
	@Column(name = "password", length = 100)
	private String Password;

	@Nullable
	@Column(name = "login_attemp")
	private long Login_attemp;

	@Nullable
	@Column(name = "is_locked")
	private Boolean Is_locked;

	@Nullable
	@Column(name = "last_login")
	private LocalDateTime Last_login;
	
	
	@NotNull
	@Column(name="created_by")
	private long Created_by;
	
	@NotNull
	@Column(name="created_on")
	private LocalDateTime Created_on;
	
	
	
	@Nullable
	@Column(name="modified_by")
	private Long Modified_by;

	@Nullable
	@Column(name="modified_on")
	private LocalDateTime Modified_on;
	
	
	@Nullable
	@Column(name="deleted_by")
	private Long Deleted_by;
	
	@Nullable
	@Column(name="deleted_on")
	private LocalDateTime Deleted_on;
	
	@NotNull
	@Column(name="is_delete",columnDefinition = "boolean default false")
	private boolean Is_delete;

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public long getLogin_attemp() {
		return Login_attemp;
	}

	public void setLogin_attemp(long login_attemp) {
		Login_attemp = login_attemp;
	}

	public Boolean getIs_locked() {
		return Is_locked;
	}

	public void setIs_locked(Boolean is_locked) {
		Is_locked = is_locked;
	}

	public LocalDateTime getLast_login() {
		return Last_login;
	}

	public void setLast_login(LocalDateTime last_login) {
		Last_login = last_login;
	}

	public long getCreated_by() {
		return Created_by;
	}

	public void setCreated_by(long created_by) {
		Created_by = created_by;
	}

	public LocalDateTime getCreated_on() {
		return Created_on;
	}

	public void setCreated_on(LocalDateTime created_on) {
		Created_on = created_on;
	}

	public long getModified_by() {
		return Modified_by;
	}

	public void setModified_by(long modified_by) {
		Modified_by = modified_by;
	}

	public LocalDateTime getModified_on() {
		return Modified_on;
	}

	public void setModified_on(LocalDateTime modified_on) {
		Modified_on = modified_on;
	}

	public long getDeleted_by() {
		return Deleted_by;
	}

	public void setDeleted_by(long deleted_by) {
		Deleted_by = deleted_by;
	}

	public LocalDateTime getDeleted_on() {
		return Deleted_on;
	}

	public void setDeleted_on(LocalDateTime deleted_on) {
		Deleted_on = deleted_on;
	}

	public boolean isIs_delete() {
		return Is_delete;
	}

	public void setIs_delete(boolean is_delete) {
		Is_delete = is_delete;
	}

	

}
