package xa.dokterhalo289.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.lang.Nullable;

@Entity
@Table(name="T_customer_wallet_withdraw")

public class T_customer_wallet_withdraw {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long Id;
	
	@ManyToOne
	@JoinColumn(name="customer_id", insertable=false, updatable=false)
	public M_customer m_customer;
	
	@NotNull
	@Column(name="customer_id")
	private Long Customer_id;
	
	@Nullable
	@Column(name="wallet_default_nominal_id")
	private Long Wallet_default_nominal_id;
	
	@NotNull
	@Column(name="amount")
	private Integer Amount;
	
	@NotNull
	@Column(name="bank_name", length = 50)
	private String Bank_name;

	@NotNull
	@Column(name="account_number", length = 50)
	private String Account_number;
	
	@NotNull
	@Column(name="account_name", length = 255)
	private String Account_name;

	@NotNull
	@Column(name="otp")
	private Integer Otp;
	
	@ManyToOne
	@JoinColumn(name="create_by", insertable=false, updatable=false)
	public M_user User_create;
	
	@NotNull
	@Column(name="create_by")
	private Long Create_by;
	
	@NotNull
	@Column(name="create_on")
	private LocalDateTime Create_on;
	
	@ManyToOne
	@JoinColumn(name="modified_by", insertable=false, updatable=false)
	public M_user User_modified;
	
	@Nullable
	@Column(name="modified_by")
	private Long Modified_by;
	
	@Nullable
	@Column(name="modified_on")
	private LocalDateTime Modified_on;
	
	@ManyToOne
	@JoinColumn(name="deleted_by", insertable=false, updatable=false)
	public M_user User_deleted;
	
	@Nullable
	@Column(name="deleted_by")
	private Long Deleted_by;
	
	@Nullable
	@Column(name="deleted_on")
	private LocalDateTime Deleted_on;
	
	@NotNull
	@Column(name="is_delete", columnDefinition="boolean default false")
	private Boolean Is_delete;
	

}
