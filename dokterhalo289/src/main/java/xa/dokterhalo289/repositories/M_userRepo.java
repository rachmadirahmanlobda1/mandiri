package xa.dokterhalo289.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import xa.dokterhalo289.models.M_user;

public interface M_userRepo extends JpaRepository<M_user, Long> {
	@Query(value = "SELECT * FROM M_user m WHERE m.email=?1 AND m.password=?2", nativeQuery = true)
    java.util.List<M_user> getLogin(String Email, String Password);
}
