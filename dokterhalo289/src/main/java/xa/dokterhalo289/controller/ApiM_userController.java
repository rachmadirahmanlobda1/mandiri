package xa.dokterhalo289.controller;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import xa.dokterhalo289.models.M_user;
import xa.dokterhalo289.repositories.M_userRepo;

@Controller
@RequestMapping(value = "user")
public class ApiM_userController {
	
	@Autowired
	M_userRepo userrepo;
	
	
	@GetMapping("/loginn")
	public ResponseEntity<List<M_user>> getAlluser() {
		try {
			List<M_user> users = this.userrepo.findAll();
			return new ResponseEntity<List<M_user>>(users, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<List<M_user>>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping(value = "ceklogin")
	ModelAndView ceklogin(@ModelAttribute M_user users, BindingResult result, HttpSession sess) { // section meyimpan
																									// selagi masi ada
		String redirect = "";
		if (!result.hasErrors()) {
			String email = (String) result.getFieldValue("Email");
			String password = (String) result.getFieldValue("Password");

			try {
				MessageDigest digest;
				digest = MessageDigest.getInstance("SHA-256"); // disini akan membandingkan password sha256
				byte[] encodehash = digest.digest(password.getBytes(StandardCharsets.UTF_8));

				List<M_user> getuser = this.userrepo.getLogin(email, password);

				try {
					sess.setAttribute("uid", getuser.get(0).getId());
					sess.setAttribute("email", getuser.get(0).getEmail());
					sess.setAttribute("created_by", getuser.get(0).getCreated_by());
					sess.setAttribute("created_on", getuser.get(0).getCreated_on());
					sess.setAttribute("is_delete", getuser.get(0).getDeleted_by());
					
					
					System.out.println("Access Granted!");
					/*if(getuser.get(0).getId()==null){ jika kita logout kembali ke root
						redirect = "redirect:/";
					}else {
						redirect = "redirect:/home";
					} */
					redirect = "redirect:/home";
				} catch (Exception e) {
					System.out.println("Access Denied!");
					redirect = "redirect:/login";
				}
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
				redirect = "redirect:/login";
			}
		}
		return new ModelAndView(redirect);
	}
}
